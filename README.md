This repository contains joint original research 
by Clifford Bergman and William DeMeo.

All work is this repository is covered by the Creative Commons 
Attribution-NoDerivatives 4.0 International License, the full text of 
which is available at:
[https://creativecommons.org/licenses/by-nd/4.0/legalcode](https://creativecommons.org/licenses/by-nd/4.0/legalcode)


