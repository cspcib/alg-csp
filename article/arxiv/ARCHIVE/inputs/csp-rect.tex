The goal of this section is to consider aspects of the Rectangularity Theorem 
that seem to limit its utility as a tool for proving tractability of \csps.
%% instance has a solution, and to prove tractability of $\CSP(\mathfrak A)$.
We first give a brief overview of the potential obstacles, and then consider
each one in more detail in the following subsections.
\begin{enumerate}
\item
  \label{item:abelian-potatoes-tractable}
  {\bf Abelian factors must have easy partial solutions.}
  One potential limitation concerns the abelian factors in the product 
  algebra associated with a \csp instance.
  Indeed, %% Corollaries \ref{cor:RT-cor} and
  Corollary~\ref{cor:RT-cor-gen} assumes that
  when the given constraint relations are projected onto abelian factors,
  we can efficiently find a solution to this restricted instance---that is,
  an element satisfying all constraint relations after projecting these
  relations onto the abelian factors of the product.  
  Section~\ref{sec:tract-abel-algebr} shows that this concern is easily
  dispensed with and is not a real limitation of
  Corollary~\ref{cor:RT-cor-gen}.

\item {\bf Intersecting products of minimal absorbing subalgebras.}
  Another potential obstacle concerns the nonabelian simple factors.
  As we saw in Section~\ref{sec:rect-theor}, the Rectangularity Theorem
  (and its corollaries) assumes that the universes of the subdirect
  products in question all intersect nontrivially
  with a single product $\myprod B_i$ of minimal absorbing subuniverses.
  (We refer to ``minimal absorbing subuniverses'' quite frequently, so from now
  on we call them \defn{masses}; that is, a \defn{mass} is 
  a {\bf m}inimal {\bf a}bsorbing {\bf s}ubuniver{\bf s}e, and the product of
  \masses will be called a \defn{mass product}.)
  Moreover, assumption~(\ref{item:RT-cor-gen-4}) of Corollary~\ref{cor:RT-cor-gen}
  requires that all constraint relations intersect nontrivially with a single
  \mas product. This is a real
  limitation, as we demonstrate in Section~\ref{sec:mass-products} below.

\item {\bf Nonabelian factors must be simple.}
  This is the most obvious limitation of the theorem and at this point
  we don't have a completely general means of overcoming it.  However,
  some methods that work in particular cases are described below.

\end{enumerate}

In the next two subsections we address potential limitations (1) and (2).
In Section~\ref{sec:var-reduc} we develop some alternative methods for
proving tractability of nonsimple algebras, and then
in Section~\ref{sec:csps-comm-idemp} we apply these methods 
in the special setting of ``commutative idempotent binars.''

\subsubsection{Tractability of abelian algebras}
\label{sec:tract-abel-algebr}
To address concern~(\ref{item:abelian-potatoes-tractable})
of the previous subsection, we observe that finite abelian algebras yield
tractable \csps.
We will show how to use this fact and the Rectangularity Theorem to find 
solutions to a given \csp instance (or determine that none exists).
To begin, recall the fundamental result of tame congruence
theory~\cite[Thm~7.12]{HM:1988}  that we 
reformulated as Lemma~\ref{lem:HM-thm-7-12}.
As noted in Remark~\ref{rem:abelian-quotients} above,
this result has the following important corollary.
(A proof that avoids tame congruence theory appears in~\cite[Thm~5.1]{MR3374664}.)
\begin{theorem}
  \label{thm:type2cp}
Let $\var{V}$ be a locally finite variety with a Taylor term. Every finite abelian member of $\var{V}$ generates a congruence-permutable variety. Consequently, every finite abelian member of $\var{V}$ is tractable.
\end{theorem}

Let $\bA$ be a finite algebra in a Taylor variety 
and fix an instance $\sI = \<\sV, \sA, \sS, \sR\>$ of $\CSP(\sansS(\bA))$
with $n = |\sV|$ variables.
Suppose all nonabelian algebras in the list $\sA$ are simple.
Let $\alpha \subseteq \nn$ denote the indices of the abelian algebras in $\sA$,
and assume without loss of generality that $\alpha = \{0,1,\dots, q-1\}$.
That is, $\bA_0$, $\bA_1$, $\dots$, $\bA_{q-1}$ are finite idempotent abelian algebras. Consider
now the restricted instance $\sI_\alpha$ obtained by dropping all constraint relations
with scopes that don't intersect $\alpha$, and by restricting the remaining constraint
relations to the abelian factors.
Since the only algebras involved in $\sI_\alpha$ are abelian, this is an instance of a
tractable \csp.  Therefore, we can check in polynomial-time whether or not $\sI_\alpha$
has a solution.
If there is no solution, then the original instance $\sI$ has no solution.
On the other hand, suppose
$f_\alpha\in \myprod_{j\in \alpha}A_j$ is a solution to $\sI_\alpha$.
In Corollary~\ref{cor:RT-cor-gen},
to reach the conclusion that the full instance has a solution, we 
required a partial solution $\bx \in \bigcap \Proj_\alpha R_\ell$.
This is precisely what $f_\alpha$ provides.

To summarize, we try to find a partial 
solution by restricting the instance to abelian factors and, if such a partial solution exists,
we use it for $\bx$ in Corollary~\ref{cor:RT-cor-gen}.
Then, assuming the remaining hypotheses of Corollary~\ref{cor:RT-cor-gen} hold,
we conclude that a solution to the original instance exists.
If no solution to the restricted instance exists, then the original instance has no solution.
Thus we see that assumption~(\ref{item:RT-cor-gen-5}) of
Corollary~\ref{cor:RT-cor-gen} does not limit the application scope of this result.

\subsubsection{Mass products}
\label{sec:mass-products}
This section concerns products of minimal absorbing subalgebras, or
``\mas products.'' Hypothesis~(\ref{item:RT-cor-gen-4}) of
Corollary~\ref{cor:RT-cor-gen} assumes that all constraint relations intersect
nontrivially with a single \mas product.
However, it's easy to contrive instances where this hypothesis does not hold.
For example, take the algebra $\bA = \<\{0,1\}, m\>$,
where $m \colon A^3 \to A$ is the ternary idempotent majority operation---that is,
$m(x,x,x)\approx x$ and $m(x,x,y)\approx m(x,y,x)\approx m(y,x,x) \approx x$.
Consider subdirect products $\bR = \<R, m\>$ and $\bS=\<S, m\>$ of
$\bA^3$ with universes
  \begin{align*}
  R &= \{(0,0,0), (0,0,1), (0,1,0), (1,0,0)\},\\
  S &= \{(0,1,1), (1,0,1), (1,1,0), (1,1,1)\}.
  \end{align*}
  Then there are \mas products that intersect nontrivially with either $R$ or $S$, 
  but no \mas product intersects nontrivially with both $R$ and $S$. 
  As the Rectangularity Theorem demands,
  each of $R$ and $S$ fully contains every \mas product that it intersects, but there is no
  single \mas product intersecting nontrivially with both $R$ and $S$. Hence,
  hypothesis~(\ref{item:RT-cor-gen-4}) of Corollary~\ref{cor:RT-cor-gen} is not
  satisfied so it would seem that we cannot use rectangularity 
  to determine whether a solution exists in such instances.
  

  The example described in the last paragraph is very special.
  For one thing, there is no solution to the instance with
  constraint relations $R$ and $S$.
  We might hope that when an instance \emph{does} have a solution,
  then there should be a solution that passes through a \mas product.
  As we now demonstrate, this is not always the case.
  In fact, Example~\ref{ex:mass-products-3} describes a case in which
  two subdirect powers intersect nontrivially,
  yet each intersects trivially with every \mas product.
\begin{proposition}
  \label{claim:mass-products-2}
There exists an algebra $\bA$ with subdirect powers $\bR$ and $\bS$ 
such that 
$R \cap S \neq \emptyset$ and, for every collection $\{B_i\}$ of \masses,
$R \cap \myprod B_i = \emptyset = S \cap \myprod B_i$.
\end{proposition}

\noindent We prove Proposition~\ref{claim:mass-products-2} by simply producing 
an example that meets the stated conditions.

\begin{example}
  \label{ex:mass-products-3}
Let $\bA = \<\{0,1,2\}, \circ\>$ be an algebra with binary operation $\circ$
given by
\vskip3mm
 \begin{center}
 \begin{tabular}{c|ccc}
      $\circ$ & 0 & 1 & 2 \\
      \hline
      0 & 0 & 1 & 2\\
      1 & 1 & 1 & 0\\
      2 & 2 & 0 & 2
    \end{tabular}
 \end{center}
\vskip3mm
The proper nonempty subuniverses of
$\bA$ are $\{0\}$, $\{1\}$, $\{2\}$, $\{0,1\}$, and $\{0,2\}$.
Note that %$B_1 = \{1\}$ and $B_2=\{2\}$
$\{1\}$ and $\{2\}$ are both minimal absorbing subuniverses with respect to the
term $t(x,y,z,w) = (x \circ y) \circ (z \circ w)$. 
Note also that $\{0\}$ is not an absorbing subuniverse of~$\bA$. For if
$\{0\}\absorbing \bA$, then
$\{0\}\absorbing \{0,1\}$ which is  false, since $\{0,1\}$ is a semilattice with
absorbing element $1$.  

Let $\bA_0 \cong \bA \cong \bA_1$, $R = \{(0,0), (1,1), (2,2)\}$, and
$S = \{(0,0), (1,2), (2,1)\}$.  Then $\bR$ and $\bS$ are subdirect products of
$\bA_0\times \bA_1$ and $R\cap S= \{(0,0)\}$.  There are four minimal absorbing subuniverses of
$\bA_0 \times \bA_1$.  They are
\[
B_{11} = \{1\}\times \{1\} = \{(1,1)\} , \; B_{12} = \{(1, 2)\}, \; B_{21} = \{(2, 1)\},
\; B_{22} = \{(2, 2)\}.
\]
Finally, observe
\[
R\cap B_{ij} = \begin{cases}
  \{(i,j)\}, & i=j,\\
  \emptyset, & i\neq j.
\end{cases}
\qquad 
S\cap B_{ij} = \begin{cases}
  \emptyset, & i=j\\
  \{(i,j)\}, & i\neq j.
\end{cases}
\]
This proves Proposition~\ref{claim:mass-products-2}.

\end{example}
