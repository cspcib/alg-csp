\begin{thebibliography}{10}

\bibitem{Barto-shanks}
L.~Barto, A rectangularity theorem for simple taylor algebras, Open Problems in
  Universal Algebra, a Shanks workshop at Vanderbilt University,
  \url{http://www.math.vanderbilt.edu/~moorm10/shanks/} (May, 2015).

\bibitem{MR2648455}
L.~Barto and M.~Kozik, Constraint satisfaction problems of bounded width,  in
  {\em 2009 50th {A}nnual {IEEE} {S}ymposium on {F}oundations of {C}omputer
  {S}cience ({FOCS} 2009)} (IEEE Computer Soc., Los Alamitos, CA, 2009) pp.
  595--603.

\bibitem{MR2953899}
L.~Barto and M.~Kozik, New conditions for {T}aylor varieties and {CSP},  in
  {\em 25th {A}nnual {IEEE} {S}ymposium on {L}ogic in {C}omputer {S}cience
  {LICS} 2010} (IEEE Computer Soc., Los Alamitos, CA, 2010) pp. 100--109.

\bibitem{MR2893395}
L.~Barto and M.~Kozik, Absorbing subalgebras, cyclic terms, and the constraint
  satisfaction problem, {\em Log. Methods Comput. Sci.} {\bf 8}(1)  (2012)
  1:07, 27.

\bibitem{MR3374664}
L.~Barto, M.~Kozik and D.~Stanovsk{\'y}, Mal'tsev conditions, lack of
  absorption, and solvability, {\em Algebra Universalis} {\bf 74}(1-2)  (2015)
  185--206.

\bibitem{MR2839398}
C.~Bergman, {\em Universal algebra}, ~{\bf 301} of {\em Pure and Applied
  Mathematics (Boca Raton)} (CRC Press, Boca Raton, FL, 2012).
\newblock Fundamentals and selected topics.

\bibitem{MR3350338}
C.~Bergman and D.~Failing, Commutative idempotent groupoids and the constraint
  satisfaction problem, {\em Algebra Universalis} {\bf 73}(3-4)  (2015)
  391--417.

\bibitem{MR2563736}
J.~Berman, P.~Idziak, P.~Markovi{\'c}, R.~McKenzie, M.~Valeriote and
  R.~Willard, Varieties with few subalgebras of powers, {\em Trans. Amer. Math.
  Soc.} {\bf 362}(3)  (2010)  1445--1473.

\bibitem{MR2137072}
A.~Bulatov, P.~Jeavons and A.~Krokhin, Classifying the complexity of
  constraints using finite algebras, {\em SIAM J. Comput.} {\bf 34}(3)  (2005)
  720--742.

\bibitem{MR2212000}
A.~A. Bulatov, A dichotomy theorem for constraint satisfaction problems on a
  3-element set, {\em J. ACM} {\bf 53}(1)  (2006)  66--120.

\bibitem{MR1630445}
T.~Feder and M.~Y. Vardi, The computational structure of monotone monadic {SNP}
  and constraint satisfaction: a study through {D}atalog and group theory, {\em
  SIAM J. Comput.} {\bf 28}(1)  (1999)  57--104 (electronic).

\bibitem{Freese2008}
R.~Freese, Computing congruences efficiently, {\em Alg. Univ.} {\bf 59}  (2008)
   337--343.

\bibitem{FreeseMcKenzie2016}
R.~Freese and R.~McKenzie, Mal'tsev families of varieties closed under join or
  mal'tsev product, {\em Algebra Universalis}   (to appear).

\bibitem{UAcalc}
R.~Freese, E.~Kiss and M.~Valeriote, Universal {A}lgebra {C}alculator  (2011),
  Available at: \url{http://uacalc.org}.

\bibitem{Freese:2009}
R.~Freese and M.~A. Valeriote, On the complexity of some {M}altsev conditions,
  {\em Internat. J. Algebra Comput.} {\bf 19}(1)  (2009)  41--77.

\bibitem{HM:1988}
D.~Hobby and R.~McKenzie, {\em The structure of finite algebras}, ~{\bf 76} of
  {\em Contemporary Mathematics} (American Mathematical Society, Providence,
  RI, 1988).

\bibitem{MR2678065}
P.~Idziak, P.~Markovi{\'c}, R.~McKenzie, M.~Valeriote and R.~Willard,
  Tractability and learnability arising from algebras with few subpowers, {\em
  SIAM J. Comput.} {\bf 39}(7)  (2010)  3023--3037.

\bibitem{MR1481313}
P.~Jeavons, D.~Cohen and M.~Gyssens, Closure properties of constraints, {\em J.
  ACM} {\bf 44}(4)  (1997)  527--548.

\bibitem{MR3076179}
K.~A. Kearnes and E.~W. Kiss, The shape of congruence lattices, {\em Mem. Amer.
  Math. Soc.} {\bf 222}(1046)  (2013)  viii+169.

\bibitem{MR2926316}
P.~Markovi{\'c}, M.~Mar{\'o}ti and R.~McKenzie, Finitely related clones and
  algebras with cube terms, {\em Order} {\bf 29}(2)  (2012)  345--359.

\bibitem{MR521057}
T.~J. Schaefer, The complexity of satisfiability problems,  in {\em Conference
  {R}ecord of the {T}enth {A}nnual {ACM} {S}ymposium on {T}heory of {C}omputing
  ({S}an {D}iego, {C}alif., 1978)} (ACM, New York, 1978) pp. 216--226.

\bibitem{Markovic:2011}
B.~B. si~\'c, J.~Jovanovi\'c, P.~Markovi\'c {\em et~al.}, {CSP} on small
  templates, \url{http://2oal.tcs.uj.edu.pl} (June, 2011).

\bibitem{MR0434928}
W.~Taylor, Varieties obeying homotopy laws, {\em Canad. J. Math.} {\bf 29}(3)
  (1977)  498--527.

\end{thebibliography}
