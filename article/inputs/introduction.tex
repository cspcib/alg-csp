The ``\csp-dichotomy conjecture'' of %% Tom{\'a}s 
Feder and %% Moshe 
Vardi~\cite{MR1630445}
asserts that every constraint satisfaction
problem (\csp) over a fixed finite constraint language is either \NP-complete or tractable.

A discovery of Jeavons, Cohen and Gyssens in~\cite{MR1481313}---later refined by Bulatov,
Jeavons and Krokhin in~\cite{MR2137072}---was the ability to transfer the question of the 
complexity of the \csp over a set of relations to a question of algebra. 
Specifically, these authors showed that the complexity of any particular \csp 
depends solely on the \emph{polymorphisms} of the constraint relations, 
that is, the functions preserving all the constraints. 
The transfer to universal algebra was made complete by Bulatov, Jeavons, and Krokhin 
in recognizing that to any set $\sR$ of constraint relations one can
associate an algebra $\bA(\sR)$ whose operations consist of the polymorphisms
of $\sR$. Following this, the \csp-dichotomy conjecture
of Feder and Vardi was recast as a universal algebra problem once it was
recognized that the conjectured sharp dividing line between those \csps that are
\NP-complete and those that are tractable was seen to depend upon
universal algebraic properties of the associated algebra. 
One such property is the existence of a Taylor term 
(defined in Section~\ref{ssec:term-ops}). Roughly speaking,
the ``algebraic \csp-dichotomy conjecture'' is the following:
The \csp associated with a finite idempotent algebra
is tractable if and only if the algebra has a Taylor term operation in its
clone. We state this more precisely as follows:

\begin{conjecture}
%% \begin{quote}  
%%   {\bf Conjecture:} 
  If $\bA$ is an algebra with a Taylor term it its
  clone and if $\sR$ is a finite set of relations compatible with $\bA$, then  
  $\CSP(\sR)$ is tractable.  Conversely, if $\bA$ is an idempotent algebra with
  no Taylor term in its clone, then there exists a finite set $\sR$ of relations
  such that $\CSP(\sR)$ is \NP-complete.
%% \end{quote}
\end{conjecture}

The second sentence of the conjecture was already
established in~\cite{MR2137072}. One goal of this paper is to provide further
evidence in support of the first sentence.

%%%%%%%%%%%%%%%%%%%
%%Start of Cliff's Introduction%%%%
%%%%%%%%%%%%%%%%%%%
Algebraists have identified two quite different techniques for proving that a finite idempotent algebra is tractable. One, often called the ``local consistency algorithm,'' works for any finite algebra lying in an idempotent, congruence-meet-semidistributive variety (\sdm for short). See~\cite{MR2648455} or~\cite{MR2893395} for details. The other, informally called the ``few subpowers technique,'' applies to any finite idempotent algebra possessing an edge term~\cite{MR2678065}. Definitions of these terms appear in Section~\ref{ssec:edge-sdm}. 

While these two algorithms cover a wide class of interesting algebras, they are not enough to resolve the above conjecture. So the question is, what is the way forward from here? One might hope for an entirely new algorithm, but none seem to be on the horizon. Alternately, we can try to combine the two existing approaches in a way that captures the outstanding cases. Several researchers have investigated this idea. In this paper, we do as well.

For example, suppose that $\bA$ is a finite, idempotent algebra possessing a congruence, $\theta$, such that $\bA/\theta$ lies in an \sdm variety and every congruence class of $\theta$ has an edge term. It is not hard to show that $\bA$ has a Taylor term. Can local consistency be combined with few subpowers to prove that $\bA$ is tractable? 

We can formalize this idea as follows. Let $\var{V}$ and $\var{W}$ be idempotent varieties. The \defn{\malcev product} of $\var{V}$ and $\var{W}$ is the class
\begin{equation*}
\var{V} \circ \var{W} = \{\,\bA : (\exists\; \theta\in \Con(\bA))\;\;\bA/\theta \in \var{W} \mathrel{\&} (\forall a\in A)\;a/\theta \in \var{V}\,\}.
\end{equation*}
$\var{V}\circ \var{W}$ is always an idempotent quasivariety, but is generally not closed under homomorphic images. A long term goal would be to prove that if all finite members of $\var{V}$ and $\var{W}$ are tractable, then the same will hold for all members of $\var{V}\circ \var{W}$. Tellingly, Freese and McKenzie show in~\cite{FreeseMcKenzie2016} that a number of important properties are preserved by \malcev product.

\begin{theorem}
\label{thm:robust}
Let $\var{V}$ and $\var{W}$ be idempotent varieties. For each of the following
properties, $P$\!, if both $\var{V}$ and $\var{W}$ have $P$\!, then so does
$\sansH(\var{V}\circ \var{W})$. 
\begin{enumerate}
\item Being idempotent;
\item
  having a Taylor term;
\item
  being \sdm;
\item
  having an edge term.
\end{enumerate}
\end{theorem}

It follows from the theorem that if both $\var{V}$ and $\var{W}$ are \sdm, or both have an edge term, then every finite member of $\sansH(\var{V}\circ \var{W})$ will be tractable. So the next step would be to consider $\var{V}$ with one of these properties and $\var{W}$ with the other. 

But even if we could prove that tractability is preserved by \malcev product, that would not be enough. In particular, simple algebras would remain problematic. Interestingly, Barto and Kozik have recently developed an approach that seems particularly well-suited to simple algebras.
In \lics'10~(\cite{MR2953899}), these researchers proved a powerful ``Absorption Theorem'' 
for products of two absorption-free algebras in a Taylor variety.
At a more recent workshop~\cite{Barto-shanks}, Barto announced further joint work with Kozik on 
a general ``Rectangularity Theorem'' 
that says, roughly, a subdirect product of simple nonabelian algebras 
contains a full product of minimal absorbing subalgebras 

In Section~\ref{sec:tayl-vari-rect} of the present paper
we state and prove a version of Barto and Kozik's Rectangularity Theorem. 
In Section~\ref{sec:csps-comm-idemp}, we apply this tool, together with some
techniques involving \malcev products and other new decomposition strategies,
to prove that every commutative, idempotent binar of cardinality at most~4 is tractable. 
