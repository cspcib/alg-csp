\subsection{Some recent history}
In late May of 2015 we attended a workshop on ``Open Problems 
in Universal Algebra,'' at which Libor Barto announced a new theorem that he 
and Marcin Kozik proved called the ``Rectangularity Theorem.'' 
At this meeting Barto gave a detailed overview of the proof~\cite{Barto-shanks}.
The authors of the present paper then made a concerted effort over many months
to fill in the details and produce a complete proof.  
Unfortunately, each attempt uncovered a gap that we were unable to fill.
Nonetheless, we found a slightly different route to the theorem.
Our argument is similar to the one presented by Barto in most of 
its key aspects. In particular, we make heavy use of the
absorption idea and the Absorption Theorem plays a key role.  
However, we were unable to complete the proof without a new 
``Linking Lemma'' (Lem.~\ref{lem:Link-2}) that we proved using a 
general result of Kearnes and Kiss.

Thus, our argument is similar in spirit to the original, but
provides alternative evidence that Barto and Kozik's Rectangularity Theorem
is correct, and may shed additional light on the result.
In the next subsection, we prove the Rectangularity Theorem 
as well as some corollaries that we use later,
in Section~\ref{sec:applications}, to demonstrate 
how the Rectangularity Theorem can be applied to \csp problems.


\subsection{Preliminaries}
We are mainly concerned with finite algebras belonging to a single Taylor
variety $\var{V}$ (though some results below hold more generally).
As mentioned, our main goal in this section is to prove the  
Rectangularity Theorem of Barto and Kozik, which can be described as follows:
Start with finite algebras
$\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$
in a Taylor variety with minimal absorbing subalgebras $\bB_i \minabsorbing \bA_i$.
Suppose at most one of these algebras is abelian and 
suppose the nonabelian algebras are simple.
Let $\bR$ be a subdirect product of $\myprod_{\nn} \bA_i$.
Then (under a few more assumptions)
if $R$ intersects nontrivially with the product $\myprod_{\nn}B_i$
of minimal absorbing subuniverses,
then $R$ contains the entire product $\myprod_{\nn}B_i$.


Before attempting to prove this, we first prove a number of useful lemmas. 
In doing so, we will make frequent use of the following

\noindent {\bf Notation:}
\begin{itemize}
\item $\nn :=\{0, 1, 2, \dots, n-1\}$.
\item For $\sigma \subseteq \nn $, 
  $A = A_0 \times A_1 \times \cdots \times A_{n-1}$,
  and $\ba = (a_0, a_1\dots, a_{n-1})$, we define
  \[
  \eta_\sigma := \ker(A \onto \Pi_\sigma A_i) = \{(\ba, \ba') \in A^2 \mid
  a_i = a_i' \text{ for all } i \in \sigma\},
  \]
  the kernel of the projection of $A$ onto coordinates $\sigma$.
\item For
  $\bR \sdp \bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}$, let 
  \[
  \etaR_\sigma := \ker(R \onto \Pi_\sigma A_i) = \{(\br, \br') \in R^2 \mid
  r_i = r_i' \text{ for all } i \in \sigma\},
  \]
  the kernel of the projection of $R$ onto the coordinates $\sigma$. Thus,
  $\etaR_\sigma = \eta_\sigma \cap R^2$.  

\item For $\sigma\subseteq \nn$, we let $\sigma' := \nn -\sigma$, and by
  $\bR \sdp \myprod_\sigma \bA_i \times \myprod_{\sigma'}\bA_i$ we 
  mean that the following three conditions 
  \ifthenelse{\boolean{footnotes}}{%
    hold:\footnote{Note that the expression
    $\bR \sdp \myprod_\sigma \bA_i \times \myprod_{\sigma'}\bA_i$ does not mean
    $\bR$ is a subalgebra of $\myprod_\sigma \bA_i \times \myprod_{\sigma'}\bA_i$. 
    Generally speaking, such an interpretation would require permuting the coordinates 
    of elements of $\bR$, which is feasible but unnecessary if we use the definition 
    given in the text above.}
  }{hold:} 
  \begin{enumerate}
  \item $\bR$ is a subalgebra of $\myprod_{\nn} \bA_i$;
  \item $\Proj_\sigma\bR = \myprod_\sigma \bA_i$;
  \item $\Proj_{\sigma'}\bR = \myprod_{\sigma'} \bA_i$;
  \end{enumerate}
  we say that $\bR$ is a \defn{subdirect product of} 
  $\myprod_\sigma \bA_i$ \emph{and} $\myprod_{\sigma'}\bA_i$ in this case.
\item 
  The subdirect product $\bR \sdp \myprod_\sigma \bA_i \times
  \myprod_{\sigma'}\bA_i$ 
  is said to be \defn{linked} if $\etaR_\sigma \join \etaR_{\sigma'} = 1_R$.
  %where $\etaR_\sigma = \ker(\bR\onto \Pi_\sigma\bA_i)$.
\item We sometimes use $\bR_\sigma$ as shorthand for $\Proj_\sigma\bR$, the projection 
  of $\bR$ onto coordinates $\sigma$.

\end{itemize}



\subsection{Rectangularity theorem}
\label{sec:rect-theor}
We are almost ready to prove the general Rectangularity Theorem.
We need just a few more results that play a crucial role in the proof.
The first comes from combining Lemma~\ref{lem:min-abs-prod}, transitivity of absorption, and 
Lemmas~\ref{lem:gen-abs1}--\ref{lem:linked-absorber}.
\begin{lemma}
\label{lem:general-linked}
Let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$
be finite algebras in a Taylor variety, 
let $\bB_i\minabsorbing \bA_i$ for each $i\in \nn $, 
and let $\nn  = \sigma \cup {\sigma'}$ be a partition of 
$\{0, 1, \dots, n-1\}$ into two nonempty disjoint subsets.
Assume
$\bR$ is a \emph{linked} subdirect product of 
$\myprod_{\sigma} \bA_i$ and $\myprod_{\sigma'}\bA_i$, and
suppose $R' := R \cap \myprod_i B_i \neq \emptyset$.
Then $\myprod_i B_i\subseteq R$, so $\bR' = \myprod_i \bB_i$.
\end{lemma}
\begin{proof}
  By Lemma~\ref{lem:sdp-general}, $\bR' \sdp \myprod_\sigma \bB_i \times \myprod_{\sigma'} \bB_i$, and
  by Lemma~\ref{lem:gen-abs1}, $\bR' \absorbing \bR$.
  Therefore, Lemma~\ref{lem:linked-absorber} implies $\bR'$ is linked. 
  By Lemma~\ref{lem:min-abs-prod} and transitivity of absorption, it follows that
  $\myprod_{\sigma} \bB_i$ and $\myprod_{\sigma'} \bB_i$ are both absorption-free, so the Absorption
  Theorem implies that $\bR' = \myprod_{\sigma}\bB_i \times \myprod_{\sigma'}\bB_i$.
\end{proof}


Next, we recall a theorem of Keith Kearnes and Emil Kiss, proved in~\cite{MR3076179}, 
about algebras in a variety that satisfies a nontrivial idempotent \malcev
condition (equivalently, has a Taylor term).
As above, we use the phrase ``Taylor variety'' to refer to an idempotent variety
with a Taylor term, and we call algebras in such varieties ``Taylor algebras.''

\begin{theorem}[\protect{\cite[Thm~3.27]{MR3076179}}] 
  \label{thm:kearnes-kiss-3.27}
  Suppose $\alpha$ and $\beta$ are congruences of a Taylor algebra. Then
  $\CC{\alpha}{ \alpha}{ \alpha \meet \beta}$ if and only if
  $\CC{\alpha \join \beta}{ \alpha \join \beta}{ \beta}$.
\end{theorem}
\noindent We use this theorem to prove a 
``Linking Lemma'' that will be central to our proof of the Rectangularity
Theorem, and the following corollary of Theorem~\ref{thm:kearnes-kiss-3.27} and 
Lemma~\ref{lem:common-meets} gives the precise context in which we will apply 
these results.
\begin{corollary}
  \label{cor:common-meets}
  Let $\alpha_0$, $\alpha_1$, $\beta$, $\delta$ be congruences of a Taylor 
  algebra $\bA$, and suppose that 
  $\alpha_0 \meet \beta = \delta = \alpha_1 \meet \beta$ and
  $\alpha_0 \join \alpha_1 = \alpha_0 \join \beta =\alpha_1 \join\beta =1_A$.
  Then, $\CC{1_A}{ 1_A}{ \alpha_0}$ and $\CC{1_A}{ 1_A}{ \alpha_1}$,
  so $\bA/\alpha_0$ and $\bA/\alpha_1$ are abelian algebras.
\end{corollary}
\begin{proof}
  The hypotheses of Lemma~\ref{lem:common-meets} hold, so
  $\CC{\beta}{\beta}{\delta}$ and 
  $\beta/\delta$ is an abelian congruence of $\bA/\delta$. 
  Now, since $\delta = \alpha_i \meet \beta$, 
  we have $\CC{\beta}{ \beta}{ \alpha_i \meet \beta}$, so 
  Theorem~\ref{thm:kearnes-kiss-3.27} implies
  $\CC{\alpha_i\join \beta}{ \alpha_i\join \beta}{ \alpha_i}$.
  This yields
  $\CC{1_A}{ 1_A}{ \alpha_i}$, 
  since $\alpha_i \join \beta =1_A$.  By 
  Lemma~\ref{lem:centralizers}~(\ref{centralizing_factors}) then,
  $\CC{1_A/\alpha_i}{ 1_A/\alpha_i}{ \alpha_i/\alpha_i}$.
  Equivalently,
  $\CC{1_{A/\alpha_i}}{ 1_{A/\alpha_i}}{ 0_{A/\alpha_i}}$. That is, $\bA/\alpha_i$ is abelian.
\end{proof}

Before stating the next result, we remind the reader that 
$k' := \nn-\{k\}$.
\begin{lemma}[Linking Lemma]
  \label{lem:Link-2}
  Let $n\geq 2$,
  let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$
  be finite algebras in a Taylor variety,
  and let $\bB_i \minabsorbing \bA_i$. Suppose
  \begin{itemize}
  \item at most one $\bA_i$ is abelian
  \item all nonabelian factors are simple
  \item $\bR \sdp \bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}$,
  \item $\etaR_i \neq \etaR_j$ for all $i\neq j$.
  \end{itemize}
  Then there exists 
  $k$ such that $\bR\sdp \bA_k \times \bR_{k'}$ is linked.
\end{lemma}
\ifthenelse{\boolean{extralong}}{%
  % wjd 2016.11.08: omitting from arxiv and journal version
  \begin{remark} 
    To be pedantic, the expression
    $\bR\sdp \bA_k \times \bR_{k'}$ that appears in the conclusion of the lemma 
    only makes sense if we swap the $0$-th and $k$-th coordinates of all elements of 
    $\bR$ so that the ``$0$-th projection'' is projection onto $\bA_k$. This is
    merely a defect of the notation.
  \end{remark}
}{}

\begin{proof}
    By way of contradiction, suppose 
    $\etaR_k \join \etaR_{k'} < 1_R$ for all $0\leq k< n$. \\[5pt]
    \noindent \underline{Case 1:} There is no abelian factor.\\
    If $n=2$, the result holds by Corollary~\ref{cor:rect-two_factors-pre}.
    Assume $n>2$.
    Since every factor is simple, each $\etaR_k$ is a coatom, so 
    $\etaR_k \join \etaR_{k'} < 1_R$ implies $\etaR_k \geq \etaR_{k'}$.  
    Then,
    \begin{equation}
      \label{eq:1111}
      \etaR_{k'}:= \Meet_{i \neq k}\etaR_i 
      = \bigl(\Meet_{i \neq k}\etaR_i\bigr) \meet \etaR_k = \etaR_{\nn} = 0_R.
    \end{equation}
    Note that (\ref{eq:1111}) holds for all $k\in \nn$.
    Now, let $\tau \subseteq \nn$ be a subset that is maximal 
    among those satisfying $\etaR_\tau > 0_R$.
    Then by what we just showed, $\tau$ omits at least two indices, say, $j$ and $\ell$.
    Therefore, by maximality of $\tau$,
    \begin{equation*}
      \etaR_\tau \meet \etaR_j = 0_R = \etaR_\tau \meet \etaR_\ell,
    \end{equation*}  
    and $\etaR_\tau \nleq \etaR_j$, and $\etaR_\tau \nleq \etaR_\ell$.
    Since all factors are simple, $\etaR_j$ and $\etaR_\ell$ are coatoms and distinct by assumption, so
    \begin{equation*}
      \etaR_\tau \join \etaR_j  = \etaR_\tau \join \etaR_\ell = \etaR_j\join \etaR_\ell = 1_R.
    \end{equation*}  
    But then, by Corollary~\ref{cor:common-meets}, both $\bA_j \cong \bR/\etaR_j$ and 
    $\bA_\ell\cong \bR/\etaR_\ell$ are abelian, which contradicts the assumption that 
    there are no abelian factors.
    \\[5pt]
    \noindent \underline{Case 2:} 
    One factor, say $\bA_0$, is abelian.\\
    For $k > 0$, $\bA_k$ is simple and nonabelian, so $\etaR_k$ is a coatom of $\Con\bR$.
    Therefore, 
    $\etaR_k \join \etaR_{k'} < 1_R$ implies
    $\etaR_{k'}\leq \etaR_k$, so
    \begin{equation}
      \label{eq:110}
      \etaR_{k'}= \etaR_{k'} \meet \etaR_k = \Meet_{i\in \nn}\etaR_i = 0_R.
    \end{equation}
    (Note that this holds for every $0<k<n$.)

    Now, let $\theta = \etaR_{0'} := \Meet_{i \neq 0} \etaR_i$.
    Let $\tau$ be a maximal subset of $0':=\{1,2,\dots, n-1\}$ such that 
    $\etaR_\tau > \theta$.  
    Obviously $\tau$ is a proper subset of $0'$. Moreover, by~(\ref{eq:110})
    there exists $j\in 0'$ such that $\etaR_\tau \nleq \etaR_j$. 
    Therefore,
    \begin{equation}
      \label{eq:12}
      \etaR_\tau \join \etaR_j = 1_R\quad \text{ and } \quad
      \etaR_\tau \meet \etaR_j = \theta.
    \end{equation}
    The first equality in (\ref{eq:12}) holds since $\etaR_j$ is a coatom,
    while the second holds since $\tau$ is a maximal set such that
    $\etaR_{\tau}> \theta$.
    By Theorem~\ref{thm:kearnes-kiss-3.27}, we have
    $\CC{\etaR_\tau \join \etaR_j}{ \etaR_\tau \join \etaR_j}{ \etaR_j}$ iff
    $\CC{\etaR_\tau}{ \etaR_\tau}{  \etaR_\tau \meet \etaR_j}$. Using (\ref{eq:12}), this means
    \[
    \CC{1_R}{ 1_R}{ \etaR_j} \quad
    \iff \quad
    \CC{\etaR_\tau}{ \etaR_\tau}{  \theta}.
    \]
    However, $\CC{1_R}{ 1_R}{ \etaR_j}$ implies $\bA_j$ is abelian, but
    $\bA_0$ is the only abelian factor, so
    \begin{equation}
      \label{imp:12}
      \CC{\etaR_\tau}{ \etaR_\tau}{  \theta} \; \text{{\it does not hold}}.  
    \end{equation}
    Since $\bA_0$ is abelian,
    $\CC{ 1_R}{ 1_R}{ \etaR_0}$; {\it a fortiori},
    $\CC{\etaR_\tau \join \etaR_0}{ \etaR_\tau \join \etaR_0}{ \etaR_0}$.
    The latter holds iff 
    $\CC{\etaR_\tau}{ \etaR_\tau}{  \etaR_\tau \meet \etaR_0}$, 
    by Theorem~\ref{thm:kearnes-kiss-3.27}.
    Now, if $\etaR_0 \meet \etaR_\tau \leq \theta$, then by
    Lemma~\ref{lem:abelian-quotients} we have
    $\CC{\etaR_\tau}{ \etaR_\tau}{ \theta}$, which is false by (\ref{imp:12}).
    Therefore, $\etaR_0 \meet \etaR_\tau \nleq \theta$.
    It follows that $\etaR_0 \meet \etaR_\tau \neq 0_R$. By~(\ref{eq:110}),
    then, there are at least two distinct indices, say, $j$ and $\ell$, in $\nn$ 
    that do not belong to $\tau$.   By maximality of $\tau$, we have
    $\etaR_\tau \meet \etaR_j = \theta = \etaR_\tau \meet \etaR_\ell$.
     By Corollary~\ref{cor:common-meets} (with $\alpha_0 = \etaR_j$, 
    $\alpha_1 = \etaR_\ell$, $\beta = \etaR_\tau$, $\delta = \theta$),
    it follows that $\bR/\etaR_j \cong \bA_j$ and $\bR/\etaR_\ell \cong
    \bA_\ell$ are both abelian---a contradiction,
    since, by assumption, the only abelian factor is~$\bA_0$.
\end{proof}
Finally, we have assembled all the tools we will use to accomplish the main goal
of this section, which is to prove the following:
\begin{theorem}[Rectangularity Theorem]
\label{thm:rectangularity}
  Let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$ be
  finite algebras in a Taylor variety
  with minimal absorbing subalgebras $\bB_i \minabsorbing \bA_i$
  and suppose
\begin{itemize}
\item at most one $\bA_i$ is abelian,
\item all nonabelian factors are simple, 
\item $\bR \sdp \bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}$,
\item $\etaR_i \neq \etaR_j$ for all $i\neq j$, %where $\etaR_i = \ker(\bR \onto \bA_i)$,
\item $R':= R \cap (B_0 \times B_1 \times \cdots \times B_{n-1}) \neq \emptyset$.
\end{itemize}
Then, $\bR' = \bB_0 \times \bB_1 \times \cdots \times \bB_{n-1}$. % \leq \bR$.
\end{theorem}


\begin{proof}
  Of course it suffices to prove
  $B_0 \times B_1 \times \cdots \times B_{n-1} \subseteq R$.
  We prove this by induction on the number of factors in the product
  $\bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}$.

  For $n=2$ the result holds by Lemma~\ref{lem:rect-two_factors}.
  Fix $n>2$ and assume that for all $2 \leq k < n$ 
  the result holds for subdirect products of 
  $k$ factors. We will prove the result holds for subdirect products of 
  $n$ factors.

  Let $\sigma$ be a nonempty proper subset of $\nn$ %:=\{0, 1, \dots, n-1\}$ 
  (so $1\leq |\sigma| < n$) and let
  $\sigma'$ denote the complement of $\sigma$ in $\nn$.
  Denote by $\bR_\sigma$ the projection of $\bR$ onto the $\sigma$-factors. 
  That is, $\bR_\sigma := \Proj_\sigma \bR$ and  $\bR_{\sigma'}:=\Proj_{\sigma'} \bR$.
  Each of these projections satisfies the assumptions
  of the theorem,
  since $\bR_{\sigma} \sdp \myprod_{\sigma}\bA_i$ and since
  for all $i\neq j$ in~$\sigma$, we have
  %\etaR_i \neq \etaR_j\; \iff \; 
  $\ker(R_{\sigma} \onto A_i) \neq \ker(R_{\sigma}\onto A_j)$.
  Similarly for $\bR_{\sigma'}$.
  Therefore, the induction hypothesis implies that %% \bB_{\sigma} :=
  $\myprod_{\sigma}\bB_i \leq \bR_{\sigma}$ and
  %% $\bB_{\sigma'} :=
  $\myprod_{\sigma'}\bB_i \leq \bR_{\sigma'}$.
  By Lemma~\ref{lem:min-abs-prod},
  $\myprod_{\sigma}\bB_i  \minabsorbing \myprod_{\sigma} \bA_i$, and
  since $\myprod_{\sigma}\bB_i \leq \bR_{\sigma}\leq \myprod_{\sigma} \bA_i$ 
  it's clear that  $\myprod_{\sigma}\bB_i \absorbing \bR_{\sigma}$.
  In fact, $\myprod_{\sigma}\bB_i \minabsorbing \bR_{\sigma}$ as well, 
  by minimality of
  $\myprod_{\sigma}\bB_i \minabsorbing \myprod_{\sigma}\bA_{i}$,
  and transitivity of absorption.
  To summarize, for every $\emptyset \subsetneqq \sigma\subsetneqq \nn$,
  \begin{equation}
    \label{eq:20}
  \bR \sdp \bR_{\sigma} \times \bR_{\sigma'}, \quad
  \myprod_\sigma \bB_i \minabsorbing \bR_{\sigma}, \quad
  \myprod_{\sigma'} \bB_i \minabsorbing \bR_{\sigma'}.
  \end{equation}

  Finally, by the Linking Lemma (Lem.~\ref{lem:Link-2}) there
  is a $k$ such that 
  $\bR \sdp \bR_{k} \times \bR_{k'}$ is linked.  Therefore,
  by Lemma~\ref{lem:general-linked} the
  the proof is complete.
\end{proof}


In case there is more than one abelian factor, we have the following slightly more general result.
Recall our notation: $\nn := \{0,1,\dots, n\}$, and if $\alpha\subseteq \nn$, then 
\[
\alpha' := \nn-\alpha \quad \text{and} \quad
\bR_\alpha := \Proj_{\alpha} \bR.\]
\begin{corollary}
\label{cor:tayl-vari-abel-fact}
Let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$ be
finite algebras in a Taylor variety 
with $\bB_i \minabsorbing \bA_i$ ($i\in \nn$),
and let $\alpha \subseteq \nn$.  Suppose
\begin{itemize}
\item $\bA_i$ is abelian for each $i \in \alpha$,
\item $\bA_i$ is nonabelian and simple for each $i \in \alpha'$,
\item $\bR \sdp \bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}$,
\item $\etaR_i \neq \etaR_j$ for all $i\neq j$, %where $\etaR_i = \ker(\bR \onto \bA_i)$,
\item $R':= R \cap (B_0 \times B_1 \times \cdots \times B_{n-1}) \neq \emptyset$.
\end{itemize}
Then $\bR'= \bR_\alpha  \times \myprod_{\alpha'}\bB_i$.
\end{corollary}
\begin{proof}
Suppose $\alpha' = \{i_0, i_1, \dots, i_{m-1}\}$.
Clearly, 
$\bR \sdp \bR_\alpha \times \bA_{i_0} \times \bA_{i_1} \times \cdots \times \bA_{i_{m-1}}$. 
If $\alpha\neq \emptyset$, then the product 
has a single abelian factor %$\Proj_{i\in \alpha} \bR \leq \myprod_{i\in \alpha} \bA_i$.
$\bR_\alpha \leq \myprod_{\alpha} \bA_i$.
Otherwise, $\alpha= \emptyset$ and the product has no abelian factors.
In either case, the result follows from Theorem~\ref{thm:rectangularity}.
\end{proof}

To conclude this section, we make two more observations that 
facilitate application of the foregoing results to \csp problems. 
\begin{corollary}
  \label{cor:RT-cor}
  Let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$ be
  finite algebras in a Taylor variety 
  with $\bB_i \minabsorbing \bA_i$ for each $i\in \nn$ and suppose
  $\bR$ and $\bS$ are subdirect products of $\myprod_{\nn} \bA_i$.
  Let $\alpha \subseteq \nn$ and assume the following:
\begin{enumerate}
\item $\bA_i$ is abelian for each $i \in \alpha$,
\item $\bA_i$ is nonabelian and simple for each $i \notin \alpha$,
\item $\etaR_i \neq \etaR_j$ for all $i\neq j$, %where $\etaR_i = \ker(\bR \onto \bA_i)$,
\item $R$ and $S$ both intersect $\myprod_{\nn} B_i$ nontrivially, 
\item there exists $\bx \in R_\alpha \cap S_\alpha$.
\end{enumerate}
Then $R \cap S \neq \emptyset$.
\end{corollary}
\begin{proof}
  By Corollary~\ref{cor:tayl-vari-abel-fact},
$\bR'= \bR_\alpha   \times \myprod_{\alpha'}\bB_i$ and
$\bS'= \bS_\alpha   \times \myprod_{\alpha'}\bB_i$. 
Therefore, since $\bx \in R_\alpha \cap S_\alpha$, we have
$\{\bx\} \times \myprod_{\alpha'}\bB_i \subseteq R \cap S$.
\end{proof}
Of course, this can be generalized to more than two subdirect products, as the 
next result states.
\begin{corollary}
  \label{cor:RT-cor-gen}
  Let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$ be
  finite algebras in a Taylor variety 
  with $\bB_i \minabsorbing \bA_i$ ($i\in \nn$).
  Suppose $\{\bR_\ell \mid 0\leq \ell < m\}$ is a set of $m$ subdirect products of 
  $\myprod_{\nn} \bA_i$.
  Let $\alpha \subseteq \nn$ and assume the following:
  \begin{enumerate}
  \item $\bA_i$ is abelian for each $i \in \alpha$,
  \item $\bA_i$ is nonabelian and simple for each $i \notin \alpha$,
  \item $\forall \ell \in \mm$, $\forall i\neq j$, 
    $\etaR^\ell_i \neq \etaR^\ell_j$ (where $\etaR^\ell_i := \ker(\bR_\ell \onto \bA_i)$),
  \item\label{item:RT-cor-gen-4} each $R_\ell$ intersects $\myprod B_i$ nontrivially, 
  \item\label{item:RT-cor-gen-5} there exists $\bx \in \bigcap \Proj_\alpha R_\ell$.
  \end{enumerate}
  Then $\bigcap R_\ell \neq \emptyset$.
\end{corollary}

