\subsection{Proof of Lemma~\ref{lem:min-abs-prod}}
\label{sec:proof-cor-min-abs-prod}
%% {\bf Lemma~\ref{lem:min-abs-prod}.} 
We prove the following: Let $\bB_i \leq \bA_i$ $(0\leq i < n)$ be algebras in the
variety $\var{V}$, let $\bB := \bB_0\times \bB_1\times \cdots \times \bB_{n-1}$, and let
$\bA := \bA_0\times \bA_1\times \cdots \times \bA_{n-1}$.
If $\bB_i\absorbing_{t_i}\bA_i$ (resp., $\bB_i\minabsorbing_{t_i}\bA_i$) for each $0\leq i < n$,
then $\bB \absorbing_s \bA$ (resp., $\bB \minabsorbing_s \bA$) 
where $s:= t_0\star t_1 \star \cdots \star t_{n-1}$.

\begin{proof}
  In case $n=2$, the fact that $\bB \absorbing_s \bA$ follows
  directly from Corollary~\ref{cor:fact2}.
  We first extend this result to \emph{minimal} absorbing subalgebras, still
  for $n=2$, and then an easy induction argument will
  complete the proof for arbitrary finite $n$.

  Assume $\bB_0 \minabsorbing_{t_0} \bA_0$ and $\bB_1 \minabsorbing_{t_1} \bA_1$.
  Then, as mentioned, Corollary~\ref{cor:fact2} implies $\bB \absorbing_s \bA$, where
  $\bB := \bB_0\times \bB_1$, $\bA := \bA_0\times \bA_1$,
  %% where $t = t_1\star \cdots \star t_n$, and $t_i = f_i$.
  and $s = t_0\star t_1$.
  %% (Below we will assume $s$ has arity $q$.)
  To show that $\bB_0\times \bB_1$ is minimal absorbing, we
  let $\bS$ be a proper subalgebra of $\bB_0\times \bB_1$ and 
  prove that $\bS$ is not absorbing in $\bA_0\times \bA_1$.
  By transitivity of absorption, it suffices to prove that 
  $\bS$ is not absorbing in $\bB_0\times \bB_1$.

  Let $t$ be an arbitrary term of arity $q$.
  For each $b \in B_1$, the set %$\{b_2\}^{-1}$
  \[
  S^{-1}b := \{b_0 \in B_0 \mid (b_0, b) \in S\}
  \]
  is easily seen to be a subuniverse of $\bB_0$.
  \ifthenelse{\boolean{arxiv}}{  
  Indeed, if $v$ is a term of arity $k$ and if $x_0, \dots, x_{k-1}$ belong to 
  $S^{-1}b$, then $(x_i, b)\in S$ and, since $\bS\leq \bA$, the element 
  \begin{align*}
    v^{\bA}((x_0, b), (x_1, b), \dots, (x_{k-1}, b)) &= 
    (v^{\bA_0}(x_0, x_1, \dots, x_{k-1}),v^{\bA_1}(b, \dots, b))\\ 
    &= (v^{\bA_0}(x_0, x_1, \dots, x_{k-1}), b)
  \end{align*}
  belongs to $S$.  Therefore, $v^{\bA_0}(x_0, x_1, \dots, x_{k-1})\in S^{-1}b$.

  Since}{Since}
  $\bS$ is a proper subalgebra, there exists $b^* \in B_1$ such that 
  $S^{-1}b^*$ is not all of $B_0$.
  Therefore, $S^{-1}b^*$
  %% $\{b_2\}^{-1}$
  is a proper subuniverse of $\bB_0$, so $S^{-1}b^*$ is not absorbing in $\bA_0$
  (by minimality of $\bB_0$). 
  Consequently,
  \begin{align*}
    \exists x_i \in S^{-1}b^*, &\quad \exists b\in B_0,
    \quad \exists j< q, \quad \exists  b'\notin S^{-1}b^* \\
    \text{ such that }  \;& t^{\bA_0}(x_0, x_1, \dots, x_{j-1}, b, x_{j+1}, \dots, x_{q-1}) = b'.
  \end{align*}
  Therefore,
  \begin{align*}
    %  \label{eq:1}
    &t^{\bA_0\times \bA_1}((x_0,b^*), %% (x_1,b^*),
    \dots, (x_{j-1}, b^*), (b, b^*), (x_{j+1}, b^*), \dots, (x_{q-1}, b^*))\\
    &= (t^{\bA_0}(x_0,%% x_1,
    \dots, x_{j-1}, b, x_{j+1}, \dots, x_{q-1}), t^{\bA_1}(b^*, \dots, b^*))= (b', b^*)
  \end{align*}
  and $(b', b^*)\notin S$ since $b' \notin S^{-1}b^*$.
  Finally, because $(x_i, b^*) \in S$ for all $i$, and since $t$ was an
  arbitrary term, it follows that $\bS$ is not absorbing in $\bB_0\times \bB_1$.

  Now fix $n>2$ and assume the result holds when there are at most $n-1$
  factors.
  Let $\bB' := \bB_0 \times \cdots \times \bB_{n-2}$ and
  $\bA' := \bA_0\times \cdots \times \bA_{n-2}$. By the induction hypothesis, 
  $\bB' \minabsorbing_{s'} \bA'$, and since
  $\bB_{n-1} \minabsorbing_{t_{n-1}} \bA_{n-1}$ we have (by 
  the $n=2$ case) 
  $\bB' \times \bB_{n-1} \minabsorbing_s \bA'\times \bA_{n-1}$,
  where $s = s' \star t_{n-1}$.
\end{proof}

\subsection{Proof of Lemma~\ref{lem:sdp-general}}
\label{sec:proof-lemma-sdp-general}

\ifthenelse{\boolean{extralong}}{%
  %%% OMITTING THE 2-fold case from the arxiv and journal version
  The argument used to prove the next lemma also works in the more
  general case of $n$-fold products.
  Although the arguments are almost identical, and although we present the
  general proof below, we begin with a proof of the $2$-fold case since it is so
  much easier to read.
  \medskip

  \noindent {\bf Lemma.}
  Let $\bA_1$ and $\bA_2$ be finite algebras in an idempotent variety, and
  suppose $\bB_i \minabsorbing \bA_i$ for $i=1,2$.
  Let $\bR \sdp \bA_1 \times \bA_2$, and let $R' = R \cap (B_1 \times B_2)$.  
  If $R'\neq \emptyset$, then $\bR' \sdp \bB_1 \times \bB_2$.

  \begin{proof}
  If $R'\neq \emptyset$, then $S_1 := \Proj_1 R'$ and 
  $S_2 := \Proj_2 R'$ are also nonempty.  
  We want to show $S_i = B_i$ for $i=1,2$.
  By minimality of $\bB_1 \minabsorbing \bA_1$ and by transitivity of
  absorption, it suffices to prove $\bS_1 \absorbing \bB_1$.
  Assume $\bB_1 \minabsorbing \bA_1$ with respect to $t$, say, $k = \ar(t)$.
  Fix $s_1, \dots, s_k \in S_1$, $b \in B_1$, and $j\leq k$.  Then
  \[
  \tilde{b} := t(s_1, \dots, s_{j-1}, b, s_{j+1}, \dots, s_k) \in B_1,
  \]
  and we must show $\tilde{b} \in S_1$. (For this will prove 
  $\bS_1 \absorbing \bB_1$.)  Since $\bR$ is subdirect, there exists $a\in A_2$
  with $(b,a) \in R$.  Also, there exist $s_1', \dots, s_k'$ in $S_2$ such that
  for all $i=1, \dots, n$ we have $(s_i, s_i')\in R'$. 
  Since all the pairs belong to $R$, the following expression is also in $R$:
  \begin{align}
    %    \label{eq:2}
    &t^{\bA_1\times \bA_2}((s_1,s_1'), \dots, (s_{j-1}, s_{j-1}'), 
    (b, a),  (s_{j+1}, s_{j+1}'), \dots, (s_k, s_k')) \nonumber \\
    &=(t^{\bA_1}(s_1, \dots, s_{j-1}, b, s_{j+1}, \dots, s_k),
    t^{\bA_2}(s_1', \dots, s_{j-1}', a, s_{j+1}', \dots, s_k')) = (\tilde{b},
    \tilde{a}).
  \end{align}
  Since $\bB_2$ is absorbing in $\bA_2$, we see that $\tilde{a}$ belongs to
  $\bB_2$. Therefore, $(\tilde{b}, \tilde{a}) \in R\cap (B_1\times B_2) = R'$, 
  which means $\tilde{b}\in S_1$, as desired. Of course, the same argument works
  to prove $S_2 = B_2$.
  \end{proof}
  As mentioned, the result generalizes to $n$-fold products and the proof is nearly identical
  to the one above.
}{}

%% \noindent {\bf Lemma~\ref{lem:sdp-general}.} %(Lem.~\ref{lem:sdp-general})
  We prove the following: Let $\bA_1$, $\dots$, $\bA_n$ be finite idempotent algebras of the same type, and
  suppose $\bB_i \minabsorbing \bA_i$ for $i=1,\dots, n$.
  Let $\bR \sdp \bA_1 \times \cdots \times \bA_n$, 
  and let $R' = R \cap (B_1 \times \cdots \times B_n)$.  
  If $R'\neq \emptyset$, then $\bR' \sdp \bB_1 \times \cdots \times \bB_n$.

\begin{proof}
  If $R'\neq \emptyset$, then for $1\leq i\leq n$ the projection 
  $S_i := \Proj_i R'$ is also nonempty.  
  We want to show $S_i = B_i$.
  By minimality of $\bB_i \minabsorbing \bA_i$ and by transitivity of
  absorption, it suffices to prove $\bS_i \absorbing \bB_i$.
  Assume $\bB_i \minabsorbing \bA_i$ with respect to $t$, say, $k = \ar(t)$.
  Fix $s_1, \dots, s_k \in S_i$, $b \in B_1$, and $j\leq k$.  Then 
  \[
  \tilde{b_i} := t^{\bA_i}(s_1, \dots, s_{j-1}, b, s_{j+1}, \dots, s_k) \in B_i,
  \]
  and we must show $\tilde{b_i} \in S_i$. (For this will prove 
  $\bS_i \absorbing \bB_i$.)  Since $\bR$ is subdirect, there exist
  $a_i \in A_i$ (for all $i\neq j$) such that 
  $\ba^\ast:= (a_1, \dots, a_{j-1}, b, a_{j+1}, \dots, a_n)\in R$. 
  Also, for each $1\leq j\leq n$ there exist $s_1^{(j)}, \dots, s_k^{(j)}$ 
  in $S_j$ such that
  for all $1\leq \ell \leq k$ we have 
  \[
  \bs_\ell := (s_\ell^{(1)},\dots, s_\ell^{(i-1)}, s_\ell, s_\ell^{(i+1)}, \dots, s_\ell^{(n)})\in R'.
  \] 
  Since all these $n$-tuples belong to $R$, the following expression is
  also in $R$: 
  \begin{align*}
    %    \label{eq:2}
    &t^{\bA_1\times \cdots \times \bA_n}(\bs_1, \dots, \bs_{j-1},\ba^\ast, \bs_{j+1}, \dots, \bs_{k})\\
    %
    &=t^{\bA_1\times \cdots \times \bA_n}((s_1^{(1)},\dots, s_1^{(i-1)}, s_1, s_1^{(i+1)}, \dots, s_1^{(n)}), \dots\\
    &\qquad \qquad \qquad  \dots, (s_{j-1}^{(1)},\dots, s_{j-1}^{(i-1)}, s_{j-1}, s_{j-1}^{(i+1)}, \dots, s_{j-1}^{(n)}),\\ 
    &\qquad \qquad \qquad\qquad \quad (a_1, \dots, a_{j-1}, b, a_{j+1}, \dots, a_n), \\
    & \qquad \qquad \qquad \qquad \quad \quad (s_{j+1}^{(1)},\dots, s_{j+1}^{(i-1)}, s_{j+1}, s_{j+1}^{(i+1)}, \dots, s_{j
+1}^{(n)}), \dots\\
    &\qquad \qquad \qquad \qquad \quad \quad \quad \dots, (s_{k}^{(1)},\dots,
    s_{k}^{(i-1)}, s_{k}, s_{k}^{(i+1)}, \dots, s_{k}^{(n)})).
  \end{align*}
This is equivalent to 
  \begin{align*}
    & \bigl( t^{\bA_1}(s_1^{(1)},\dots,s_{j-1}^{(1)},a_1, s_{j+1}^{(1)},\dots,s_{k}^{(1)}),\dots \\
    & \qquad \qquad \qquad\quad \dots, t^{\bA_{i-1}}(s_1^{(i-1)}, \dots, s_{j-1}^{(i-1)}, a_{j-1}, s_{j+1}^{(i-1)}, \dots, 
s_{k}^{(i-1)}),\\
    & \qquad \qquad \qquad\quad \quad\quad \quad t^{\bA_{i}}(s_1, \dots, s_{j-1}, b, s_{j+1}, \dots, s_{k}),\\
    & \qquad \qquad \qquad\quad \quad \quad \quad \quad t^{\bA_{i+1}}(s_1^{(i+1)}, \dots, s_{j-1}^{(i+1)}, a_{j+1}, s_{j
+1}^{(i+1)}, \dots, s_{k}^{(i+1)}), \dots\\
    & \qquad \qquad \qquad\quad \quad \quad \quad \quad \quad  \dots t^{\bA_n}(s_1^{(n)}, \dots, s_{j-1}^{(n)}, a_n, 
s_{j+1}^{(n)}, \dots, s_{k}^{(n)}) \bigr),
    %% &= (\tilde{b_1},\dots, \tilde{b_{n}}).
  \end{align*}
which reduces to $(\tilde{b_1},\dots, \tilde{b_{n}})$.
  Since $\bB_i$ is absorbing in $\bA_i$, we see that 
  $(\tilde{b_1},\dots, \tilde{b_{n}})\in (B_1 \times \cdots \times B_n)$.
  Therefore, 
  $(\tilde{b_1},\dots, \tilde{b_{n}})\in R\cap B_1 \times \cdots \times B_n$,
  which means $\tilde{b_i}\in S_i$, as desired. Of course, the same argument 
  works for all $1\leq i\leq n$. 
\end{proof}


\ifthenelse{\boolean{arxiv}}{%
  %% wjd (20161108): omitting this proof from the journal version of the paper
\subsection{Proof of Lemma~\ref{lem:abelian-AF}}
\label{sec:proof-that-abelian}
An very useful property of abelian algebras is that they are absorption-free.
A proof of this appears in~\cite[Lem~4.1]{MR3374664}, but we include 
a proof in this section for easy reference and to keep the paper somewhat self-contained.
First we require an elementary fact about functions on finite sets.\\[4pt]
\noindent {\bf Fact A.1.}
%% \begin{fact}
%% \label{fact:idemp-funct-fin-set}
  If $f\colon X \rightarrow X$ is a (unary) function on a finite set $X$, then there
  is a natural number $k\geq 1$ 
  such that the $k$-fold composition of $f$ with itself
  is the same function as the $2k$-fold composition.  That is, for all 
  $x \in X$, $f^{2k}(x) = f^k(x)$.\\[6pt]
\noindent {\bf Lemma~\ref{lem:abelian-AF}.}
Finite idempotent abelian algebras are absorption-free.

\begin{proof}
  Suppose $\bA$ is a finite idempotent abelian algebra with $\bB \absorbing_t \bA$.
  We show $\bB = \bA$.
  If $t$ is unary, then by idempotence $t$ is the identity function and
  absorption in this case means $t[A] \subseteq B$.  It follows that $A = B$ and
  we're done.  So assume $t$ has arity $k>1$.  We
  will show that there must also be a $(k-1)$-ary term operation 
  $s\in \sansClo(\bA)$ such that $\bB \absorbing_s \bA$.  
  It follows inductively that there must also be a unary absorbing term
  operation. Since a unary idempotent operation is the identity
  function, this will complete the proof.

  Define a sequence of terms $t_0, t_1, \dots$ as follows:
  for each $\bx = (x_1, \dots, x_{k-1}) \in A^{k-1}$ and $y\in A$,
  \begin{align*}
    t_0(\bx, y) &= t(\bx, y),\\
    t_1(\bx, y) &= t(\bx, t_0(\bx, y)) = t(\bx, t(\bx, y)), \\
    t_2(\bx, y) &= t(\bx, t_1(\bx, y)) = t(\bx, t(\bx, t(\bx, y))),\\
     &\vdots\\
    t_m(\bx, y) &= t(\bx, t_{m-1}(\bx, y)) 
                 = t(\bx, \dots, t(\bx, t(\bx, t(\bx, y))) \cdots )).
  \end{align*}
  It is easy to see that $\bB$ is absorbing in $\bA$ with respect to $t_m$, that
  is, $\bB \absorbing_{t_m} \bA$.  

  For each $\bx_i\in A^{k-1}$, 
  define $p_i:A \rightarrow A$ by $p_i(y) = t(\bx_i, y)$.  Then, 
  $p_i^m(y) = t_m(\bx_i, y)$, so by
  Fact~A.1 %% \ref{fact:idemp-funct-fin-set}
  there exists an $m_i\geq 1$ such that 
   $p_i^{2m_i} = p_i^{m_i}$.  That is, 
  $t_{m_i}(\bx_i,t_{m_i}(\bx_i,y)) = t_{m_i}(\bx_i,y)$.
  Let $m$ be the product of all the $m_i$ as $\bx_i$ varies over $A^{k-1}$.
  Then, for all $\bx_i \in A^{k-1}$, we have 
   $p_i^{2m} = p_i^{m}$.  Therefore, 
  for all $\bx \in A^{k-1}$, we have 
  $t_{m}(\bx,t_{m}(\bx,y)) = t_{m}(\bx,y)$.

  We now show that the $(k-1)$-ary term operation $s$, defined for all 
  $x_1, \dots, x_{k-1} \in A$ by 
  \[
  s(x_1, \dots, x_{k-2}, x_{k-1})=
  t(x_1, \dots, x_{k-2}, x_{k-1}, x_{k-1})
  \]
  is absorbing for $\bB$, that is, $\bB \absorbing_{s} \bA$.  It suffices to
  prove that $s[B \times \cdots \times B \times A] \subseteq B$. 
  (For if the factor involving $A$ occurs earlier, we appeal to
  absorption with respect to $t$.)
  So, for $\bb \in B^{k-2}$ and $a\in A$, we will show 
  $s(\bb, a) = t_m(\bb, a,a) \in B$.  For all $b\in B$, we have
  \[
  t_m(\bb, b, a) = t_m(\bb, b, t_m(\bb, b, a)).
  \]
  Therefore, if we apply (at the $(k-1)$-st coordinate) 
  the fact that $\bA$ is abelian, then we have
  \begin{equation}
    \label{eq:14}
    t_m(\bb, a, a) = t_m(\bb, a, t_m(\bb, b, a)).
  \end{equation}
  By absorption, $t_m(\bb, b, a)$ belongs to $B$, thus so does the
  entire expression on the right of~(\ref{eq:14}).  This proves that 
  $s(\bb, a) = t_m(\bb, a,a) \in B$, as desired.
\end{proof}
}{}


\ifthenelse{\boolean{arxiv}}{
  %% wjd (20161108): omitting this proof from the journal version of the paper
  \subsection{Direct Proof of Corollary~\ref{cor:fry-pan}}
  \label{sec:proof-fry-pan-cor}

%% {\bf Corollary~\ref{cor:fry-pan}.}
  We prove the following:  Let $\bA_0, \dots, \bA_{n-1}$ be finite
  idempotent algebras in a Taylor variety and suppose $\bR \sdp \prod \bA_i$.
  For some $0< k < n-1$, assume the following:
  \begin{itemize}
  \item if $0\leq i < k$ then $\bA_i$ is abelian;
  \item if $k\leq i < n$ then $\bA_i$ has a sink $s_i \in A_i$.
  \end{itemize}
  Then
   $Z := R_{\kk}
  %% \Proj_{\kk}R 
  \times \{s_k\} \times \{s_{k+1}\} \times \cdots \times \{s_{n-1}\}  \subseteq R$,
  where $R_{\kk} = \Proj_{\kk}R$.

\begin{proof}
  Since $\bA_{\kk} := \prod_{i<k} \bA_i$ is abelian and lives in a Taylor variety, 
  there exists a term $m$ such that $m^{\bA_{\kk}}$ is a \malcev term operation on
  $\bA_{\kk}$ (Theorem~\ref{thm:type2cp}).  Since we are working with idempotent terms, we can be sure 
  that for each $i\in \nn$ the term operation $m^{\bA_i}$ is not
  constant (so depends on at least one of its arguments).

  Fix $\bz :=(r_0, r_1, \dots, r_{k-1}, s_k, s_{k+1}, \dots, s_{n-1}) \in Z$.
  We will show that $\bz \in R$.
  Since $\bz_{\kk}\in R_{\kk}$, there exists $\br \in R$ 
  whose first $k$ elements agree with those of $\bz$.  
  That is, $\br_{\kk} =  (r_{0}, r_{1}, \dots, r_{k-1}) = \bz_{\kk}$.

  Now, since $\bR$ is subdirect, there exists $\bx^{(0)} \in R$ such that 
  $\bx^{(0)}(k) = s_k$, the sink in $\bA_k$.
  If the term operation $m^{\bA_k}$ depends on its second or third argument,
  consider $\vy^{(0)}= m(\br, \bx^{(0)}, \bx^{(0)}) \in R$.  
  (Otherwise, $m^{\bA_k}$ depends on its
  first argument, so consider $\vy^{(0)}= m(\bx^{(0)}, \bx^{(0)}, \br)$.)
  For each $0\leq i < k$ we have 
  $\vy^{(0)}(i) = m^{\bA_i}(r_i, \bx^{(0)}(i), \bx^{(0)}(i)) = r_i$,
  since $m^{\bA_i}$ is \malcev.   Thus, $\vy^{(0)}_{\kk} = \bz_{\kk}$.
  At index $i = k$,  we have
  $\vy^{(0)}(k) = m^{\bA_k}(r_k, s_k, s_k) = s_k$, since $s_k$ is a sink in $\bA_k$.
  By the same argument, but starting with $\bx^{(1)} \in R$ such that
  $\bx^{(1)}(k+1) = s_{k+1}$, there exists $\vy^{(1)} \in R$ such that
  $\vy^{(1)}_{\kk} = \bz_{\kk}$ and $\vy^{(1)}(k+1) = s_{k+1}$.

  Let $t$ be any term of arity $\ell\geq 2$ that depends on at least two of its 
  arguments, say, arguments $p$ and $q$, and 
  consider $t(\vy^{(0)}, \dots, \vy^{(0)},\vy^{(1)}, \vy^{(0)}, \dots, \vy^{(0)})$, where 
  $\vy^{(1)}$ appears as argument $p$ (or $q$) and $\vy^{(0)}$ appears elsewhere.
  By idempotence, and by the fact that $s_k$ and $s_{k+1}$ are sinks, we have
  %% \[
  %% t\left( \begin{array}{cccccccc}
  %%   (r_0,&\dots,&r_{k-1},&s_k,&\ast,&\ast,& \dots,&\ast)\\
  %%     & & \vdots& &&\\
  %%   (r_0,&\dots,&r_{k-1},&s_k,&\ast,&\ast,& \dots,&\ast)\\
  %%   (r_0,& \dots,& r_{k-1},&\ast,&s_{k+1},&\ast,& \dots,& \ast)\\
  %%   (r_0,&\dots,&r_{k-1},&s_k,&\ast,&\ast,& \dots,&\ast)\\
  %%      & &\vdots & &&\\
  %%   (r_0,&\dots,&r_{k-1},&s_k,&\ast,&\ast,& \dots,&\ast)\\
  %% \end{array}\right) = (r_0, \dots, r_{k-1}, s_k, s_{k+1}, \ast, \dots, \ast),
  %% \]
  \[
  t\left( \begin{array}{cccc}
    (r_0,\dots,r_{k-1},&s_k,&\ast,&\ast, \dots,\ast)\\
        \vdots & &&\vdots\\
    (r_0,\dots,r_{k-1},&s_k,&\ast,&\ast, \dots,\ast)\\
    (r_0, \dots, r_{k-1},&\ast,&s_{k+1},&\ast, \dots, \ast)\\
    (r_0,\dots,r_{k-1},&s_k,&\ast,&\ast, \dots,\ast)\\
        \vdots  && &\vdots\\
    (r_0,\dots,r_{k-1},&s_k,&\ast,&\ast, \dots,\ast)\\
  \end{array}\right) = (r_0, \dots, r_{k-1}, s_k, s_{k+1}, \ast, \dots, \ast),
  \]
  where the wildcard $\ast$ represents unknown elements.
  Denote this element of $R$ by
  $\br^{(1)} = (r_0, \dots, r_{k-1}, s_k, s_{k+1}, \ast, \dots, \ast)$.
  Continuing as above, we find 
$\vy^{(2)} = (r_0, \dots, r_{k-1}, \ast, \ast, s_{k+2}, \ast, \dots, \ast) \in R$, 
and compute 
\begin{align*}
\br^{(2)}:= t(\br^{(1)}, \dots, \br^{(1)}, & \, \vy^{(2)}, \br^{(1)}, \dots, \br^{(1)}) = 
(r_0, \dots, r_{k-1}, s_k, s_{k+1}, s_{k+2}, \ast, \dots, \ast),\\
%&\uparrow\\
&\; ^{\widehat{\lfloor}} \, \text{$p$-th argument}
\end{align*}
which also belongs to $R$.  In general, once we have 
\begin{align*}
\br^{(j)}&:= (r_0,  \dots, r_{k-1}, s_k, \dots, s_{k+j}, \ast, \dots, \ast) \in R, \text{ and }\\
\vy^{(j+1)} &:= (r_0,  \dots, r_{k-1}, \ast, \dots, \ast, s_{k+j+1}, \ast, \dots, \ast) \in R,
\end{align*} 
we compute 
\begin{align*}
\br^{(j+1)} &= t(\br^{(j)},  \dots, \br^{(j)}, \vy^{(j+1)}, \br^{(j)}, \dots, \br^{(j)}) \\
              & = (r_0,  \dots, r_{k-1}, s_k, \dots, s_{k+j+1}, \ast, \dots, \ast) \in R.  
\end{align*}
Proceeding inductively in this way yields $\bz =
(r_0, \dots, r_{k-1}, s_k, \dots, s_{n-1}) \in R$, as desired.
\end{proof}

}{}




\ifthenelse{\boolean{arxiv}}{
  %% wjd (20161108): omitting this proof from the journal version of the paper

\subsection{Other elementary facts}
The remainder of this section collects some observations that can be useful when
trying to prove that an algebra is abelian.  We have moved the statements and
proofs of these facts to the appendix since we didn't end up using any of them
in the paper.

Denote the diagonal of $A$ by $D(A) := \{(a,a)\mid a \in A\}$. 

\begin{lemma}
\label{lem:diagonal}
An algebra $\bA$ is abelian if and only if there is some $\theta \in \Con (\bA^2)$ that has
the diagonal set $D(A)$ as a congruence class.
\end{lemma}
\begin{proof}
  ($\Leftarrow$) Assume $\Theta$ is such a congruence.  Fix 
  $k<\omega$,
  $t^{\bA}\in \sansClo_{k+1}(\bA)$, 
  $u, v \in A$, and
  $\bx, \vy \in A^k$.
  We will prove the implication~(\ref{eq:22}), which in the present context is
  \begin{equation*}
    t^\bA(\bx,u) = t^\bA(\vy,u) \quad \Longrightarrow \quad 
    t^{\bA}(\bx,v) = t^{\bA}(\vy,v).
  \end{equation*}
  Since $D(A)$ is a class of $\Theta$, we have 
  $(u,u) \mathrel{\Theta} (v,v)$, and since $\Theta$ is a reflexive relation, we have
  $(x_i,y_i)  \mathrel{\Theta} (x_i,y_i)$ for all $i$.  Therefore,
  \begin{equation}
    \label{eq:9}  
    t^{\bA\times \bA}((x_1,y_1), \dots, (x_k,y_k), (u,u))
    \mathrel{\Theta}
    t^{\bA\times \bA}((x_1,y_1), \dots, (x_k,y_k), (v,v)).
  \end{equation}
  since $t^{\bA \times \bA}$ is a term operation of $\bA\times \bA$.
  Note that~(\ref{eq:9}) is equivalent to
  \begin{equation}
    \label{eq:13}
    (t^{\bA}(\bx, u), t^{\bA}(\vy,u))
    \mathrel{\Theta}
    (t^{\bA}(\bx, v), t^{\bA}(\vy, v)).
  \end{equation}
  If $t^{\bA}(\bx, u)= t^{\bA}(\vy, u)$ then 
  the first pair in~(\ref{eq:13}) belongs to the $\Theta$-class
  $D(A)$, so the second pair must also belong this $\Theta$-class.
  That is, $t^{\bA}(\bx, v)= t^{\bA}(\vy, v)$, as desired.

  \vskip2mm

  \noindent ($\Rightarrow$) Assume $\bA$ is abelian. We show
  $\Cg^{\bA^2}(D(A)^2)$ has $D(A)$ as a block.  Assume
  \begin{equation}
    \label{eq:16}
  ((x,x), (c,c')) \in \Cg^{\bA^2}(D(A)^2).
  \end{equation}
  It suffices to prove that $c=c'$.  Recall, \malcev's congruence generation
  theorem states that (\ref{eq:16}) holds iff
  \begin{align*}
  \exists \,& (z_0,z_0'), (z_1,z_1'), \dots, (z_n,z_n') \in A^2\\
    \exists \,& ((x_0,x_0'), (y_0,y_0')), ((x_1,x_1'), (y_1,y_1')), \dots, 
    ((x_{n-1},x_{n-1}'), (y_{n-1},y_{n-1}')) \in D(A)^2\\
    \exists \, & f_0, f_1, \dots, f_{n-1}\in F^*_{\bA^2}
  \end{align*}
  such that 
  \begin{align}
    \label{eq:7}
    \{(x, x),(z_1,z_1')\} &= \{f_0(x_0,x_0'), f_0(y_0,y_0')\}\\
    \nonumber
     \{(z_1,z_1'),(z_2,z_2')\} &= \{f_1(x_1,x_1'), f_1(y_1,y_1')\}\\
    \nonumber
     & \vdots\\
    \label{eq:8}
     \{(z_{n-1},z_{n-1}'),(c, c')\} &= \{f_{n-1}(x_{n-1},x_{n-1}'), f_{n-1}(y_{n-1},y_{n-1}')\}
  \end{align}
  The notation $f_i\in F^*_{\bA^2}$ means 
  \begin{align*}
    f_i(x, x') &= g_i^{\bA^2}((a_1, a_1'), (a_2, a_2'), \dots, (a_k, a_k'), (x, x'))\\
               &= (g_i^{\bA}(a_1, a_2, \dots, a_k, x), g_i^{\bA}(a_1', a_2', \dots, a_k', x')),
  \end{align*}
  for some $g_i^{\bA} \in \sansClo_{k+1}(\bA)$ and some constants 
  $\ba = (a_1, \dots, a_k)$ and $\ba' = (a_1', \dots, a_k')$ in $A^k$. 
  Now, $((x_i,x_i'), (y_i,y_i'))\in D(A)^2$ implies 
  $x_i=x_i'$, and $y_i=y_i'$, so in fact we have 
  \[
    \{(z_i,z_i'),(z_{i+1},z_{i+1}')\} = \{f_i(x_i,x_i), f_i(y_i,y_i)\} \quad (0\leq i < n).
  \]
  Therefore, by Equation~(\ref{eq:7}) we have either 
  \[
    (x,x)= (g_i^{\bA}(\ba, x_0), g_i^{\bA}(\ba', x_0)) \quad \text{ or } \quad 
    (x,x)= (g_i^{\bA}(\ba, y_0), g_i^{\bA}(\ba', y_0)).
  \]
  Thus, either $g_i^{\bA}(\ba, x_0) =  g_i^{\bA}(\ba', x_0)$ %\quad \text{ or } \quad 
  or $g_i^{\bA}(\ba, y_0) =  g_i^{\bA}(\ba', y_0)$.
  By the abelian assumption, if one of these equations holds, then so does the
  other. This and and Equation (\ref{eq:7}) imply $z_1 = z_1'$.  Applying the same
  argument inductively, we find that $z_i = z_i'$ for all $1\leq i < n$ and so, by
  (\ref{eq:8}) and the abelian property, we have $c= c'$.
\end{proof}

Lemma~\ref{lem:diagonal} can be used to prove the next result
which states that if there is a congruence of $\bA_1 \times \bA_2$ that has the
graph of a bijection between $A_1$ and $A_2$ as a block, then both $\bA_1$ and
$\bA_2$ are abelian algebras.

\begin{lemma}
  \label{lem:bijection_abelian}
  Suppose $\rho \colon A_0 \to A_1$ is a bijection and suppose the graph
  $\{(x, \rho x) \mid x \in A_0\}$ is a block of some congruence
  $\beta \in \Con (A_0 \times A_1)$.  Then both $\bA_0$ and $\bA_1$ are abelian.
\end{lemma}
\begin{proof}
  Define the relation $\alpha\subseteq (A_1\times A_1)^2$ as follows: for
  $((a,a'), (b,b')) \in (A_1\times A_1)^2$,
  \[
  (a,a')\mathrel{\alpha} (b,b')
  \quad \iff \quad
  (a, \rho a') \mathrel{\beta} (b, \rho b')
  \]
  We prove that the diagonal $D(A_1)$ is a block of $\alpha$ by showing that
  $(a, a) \mathrel{\alpha} (b,b')$ implies $b = b'$.
  Indeed, if $(a, a) \mathrel{\alpha} (b,b')$, then
  $(a, \rho a) \mathrel{\beta} (b, \rho b')$, which means that
  $(b, \rho b')$ belongs to the block and
  $(a, \rho a)/\beta = \{(x, \rho x)\mid x\in A_1\}$.  Therefore,
  $\rho b  = \rho b'$, so $b = b'$ since $\rho$ is injective.
  This proves that $\bA_1$ is abelian.

  To prove $\bA_2$ is abelian, we reverse the roles of $A_1$ and $A_2$ in the
  foregoing argument.  
  If $\{(x, \rho x) \mid x \in A_1\}$ is a block of $\beta$,
  then 
  $\{(\rho^{-1}(\rho x), \rho x) \mid \rho x \in A_2\}$ is a block of $\beta$; that
  is, $\{(\rho^{-1} y, y) \mid y \in A_2\}$ is a block of $\beta$.  Define 
  the relation $\alpha\subseteq (A_2\times A_2)^2$ as follows: for
  $((a,a'), (b,b')) \in (A_2\times A_2)^2$,
  \[
  (a,a')\mathrel{\alpha} (b,b')
  \quad \iff \quad
  (\rho^{-1}a, \rho a') \mathrel{\beta} (\rho^{-1}b, \rho b').
  \]
  As above, we can prove that the diagonal $D(A_2)$ is a block of $\alpha$
  by using the injectivity of $\rho^{-1}$ to show that $(a, a) \mathrel{\alpha}
  (b,b')$
  implies $b = b'$.
\end{proof}


\begin{lemma}
\label{lem:triv-clone-implies-abelian}
If $\sansClo(\bA)$ is trivial (i.e., generated by the projections),
then $\bA$ is abelian.
\end{lemma}
(In fact, it can be shown that $\bA$ is \emph{strongly abelian} in this case. 
We won't need this stronger result, and the proof that $\bA$ is abelian is elementary.)
\begin{proof}
We want to show $\C{1_A}{1_A}$.  Equivalently, we must show
that for all $t\in \sansClo(\bA)$ (say, $(\ell+m)$-ary) 
and all $a, b \in A^\ell$, we have $\ker t(a,\cdot)=\ker t(b,\cdot)$.
We prove this by induction on the height of the term $t$.  Height-one terms are
projections and the result is obvious for these.  Let $n>1$ and assume the result
holds for all terms  of height less than
$n$.  Let $t$ be a term of height $n$, say, $k$-ary.  Then for some terms 
$g_1, \dots, q_k$ of height less than $n$ and for some $j\leq k$, we have
$t = p^k_j [q_1, q_2, \dots, q_k] = q_j$ and since $q_j$ has height less than
$n$, we have
\[
\ker t(a,\cdot)=\ker g_j(a,\cdot) = \ker g_j(b,\cdot)=\ker t(b,\cdot).
\]\end{proof}
}{}





